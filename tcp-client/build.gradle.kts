plugins{
    kotlin("jvm") version "1.6.21"
}

dependencies{
    implementation(project(":domain"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.1")
}