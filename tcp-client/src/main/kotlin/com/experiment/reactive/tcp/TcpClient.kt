package com.experiment.reactive.tcp

import com.experiment.reactive.RequestResponseClient
import com.experiment.reactive.ResponseType
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.SocketChannel
import java.util.concurrent.ConcurrentHashMap

class TcpClient : RequestResponseClient {
    lateinit var socketChannel: SocketChannel
    lateinit var selector: Selector
    val mutex = Mutex()
    val requests = ConcurrentHashMap<Int, CompletableDeferred<ByteArray>>()

    suspend fun initialize() {
        socketChannel = withContext(Dispatchers.IO) {
            SocketChannel.open()
            socketChannel.configureBlocking(false)
            socketChannel.connect(InetSocketAddress("localhost", 1234))
            while (!socketChannel.finishConnect()) {
                delay(1)
            }
            selector = Selector.open()
            socketChannel.register(selector, SelectionKey.OP_READ)
            socketChannel
        }
    }

    fun readResponses() {
        val buffer = ByteBuffer.allocate(1024)
        while (true) {
            selector.select()
            val selectedKeys = selector.selectedKeys()
            val iter = selectedKeys.iterator()
            while (iter.hasNext()) {
                val key = iter.next()
                if (key.isReadable) {
                    val channel = key.channel() as SocketChannel
                    channel.read(buffer)
                    val (requestId, response) = extractHeader(buffer)
                    requests[requestId]?.complete(response)
                }
            }
        }
    }

    private fun extractHeader(buffer: ByteBuffer): Pair<Int, ByteArray> {
        TODO("Not yet implemented")
    }

    override suspend fun request(payload: ByteArray): ResponseType {
        val (requestId, message) = addHeader(payload)
        try {
            val result = withTimeout(5000) {
                val job = CompletableDeferred<ByteArray>()
                mutex.withLock {
                    requests[requestId] = job
                    // send here
                }
                job.await()
            }
            return ResponseType.Success(result)
        } catch (ex: TimeoutCancellationException) {
            return ResponseType.Failure("timeout")
        } finally {
            requests.remove(requestId)
        }
    }

    private fun addHeader(payload: ByteArray): Pair<Int, ByteArray> {
        TODO("Not yet implemented")
    }
}