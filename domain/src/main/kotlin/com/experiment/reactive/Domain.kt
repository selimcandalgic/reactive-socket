package com.experiment.reactive

interface RequestResponseClient {
    suspend fun request(payload: ByteArray): ResponseType
}

sealed class ResponseType {
    class Success(val payload: ByteArray) : ResponseType()
    class Failure(val reason: String) : ResponseType()
}